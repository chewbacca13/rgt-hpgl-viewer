from PyQt5.QtGui import QPen, QPainterPath, QPolygonF, QPainter
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsPolygonItem
from PyQt5.QtCore import Qt, QPointF

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Figure(QGraphicsPolygonItem):

    def __init__(self, x, y):
        super().__init__()
        self.labels = []

    def addPoint(self, P):
        self.setPolygon(self.polygon() << P)

    def addLabel(self, label):
        self.labels.append(label)
