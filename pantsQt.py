#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QWidget, QGraphicsScene, QGraphicsView, QGraphicsItem
from PyQt5.QtGui import QPen, QBrush
from PyQt5.Qt import Qt
from PyQt5.QtCore import QRectF

from HPGL_Scene import *

import sys

class Main(QWidget):
    
    def __init__(self, arg):
        super().__init__()
        self._height = 600
        self._width = 800
        self.zoom = 1
        # self.file = open("b.hpgl", "r")
        self.file = open(arg[1], "r")

        self.hpgl_scene = None
        self.graphicView = None
        self.painter = None

        self.id = 0

        self.initUI()
        
        
        
    def initUI(self):      

        self.setGeometry(150, 150, self._width, self._height)
        self.setWindowTitle('HPGL Pants')
        self.createGraphicView()
        self.show()

    def createGraphicView(self):
        self.hpgl_scene = HPGL_Scene()
        self.graphicView = QGraphicsView(self.hpgl_scene, self)
        self.graphicView.setGeometry(0, 0, self._width, self._height)
        self.hpgl_scene.draw_hpgl(self.file)


    def colorFigure(self, id):
        self.hpgl_scene.colorFigure(id)


    def resizeEvent(self, event):
        QWidget.resizeEvent(self, event)
        self.graphicView.resize(self.width(), self.height())
        self.fitInWindow()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Plus:
            zoom = 1.1
            self.graphicView.scale(zoom, zoom)
        if event.key() == QtCore.Qt.Key_Minus:
            zoom = 0.9
            self.graphicView.scale(zoom, zoom)
        if event.key() == QtCore.Qt.Key_0:
            self.graphicView.fitInView(self.hpgl_scene.sceneRect(), QtCore.Qt.KeepAspectRatioByExpanding)
        if event.key() == QtCore.Qt.Key_C:
            self.colorFigure(self.id)
            # print(self.id)
            self.id += 1

    def fitInWindow(self):
        self.graphicView.fitInView(self.hpgl_scene.sceneRect(), QtCore.Qt.KeepAspectRatioByExpanding)

   
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Main(sys.argv)
    ex.fitInWindow()
    sys.exit(app.exec_())