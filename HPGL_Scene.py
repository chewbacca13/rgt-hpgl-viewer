from PyQt5.QtGui import QPainter, QPen, QFont, QBrush
from PyQt5.QtWidgets import QGraphicsScene
from PyQt5.QtCore import Qt, QPoint

from Figure import Figure, Point

import math
import sys
import re
import subprocess


class HPGL_Scene(QGraphicsScene):

    def __init__(self):
        super().__init__()
        self.bounding_rect = None
        self.figures = []
        self.figures_all = []

        #saving the labels then adding them at the end
        self.labels = []

    def draw_hpgl(self, file):
        x, y = 0, 0
        text = " "

        while text != "":
            text = file.readline().replace(chr(3), '')
            if len(text) < 2:
                break
            com = text[0:2]

            if com == 'PU':
                # there's a coord so move the pen there
                if len(text) > 4 and text[2] != ';':
                    aux = text[2:-1].split(",")
                    x = int(aux[0])
                    y = -int(aux[1])
                    self.figures_all.append(Figure(x, y))
                    self.figures_all[-1].addPoint(QPoint(x, y))

            elif com == 'PD':
                aux = text[2:-1].split(",")
                x = int(aux[0])
                y = -int(aux[1])
                self.figures_all[-1].addPoint(QPoint(x, y))

            elif com == 'LB':
                label = self.addText(re.sub('\n', '', text[2:-1]), QFont("Times", 550, QFont.Bold))
                label.setPos(x, y)
                # remove the last figure beacuse it gonna be a label
                self.figures_all.pop()
                self.labels.append(label)
                
            else:
                pass        

        #drawing the polygons
        pen = QPen(Qt.black)
        pen.setWidth(60)

        for fig in self.figures_all:
            fig.setPen(pen)
            self.addItem(fig)

        #clean figures/ select selectable
        self.removeBoundingFig()
        self.addLabels2Fig()
        
    def insideFigure(self, x, y):
        for fig in self.figures_all:
            if fig.contains(QPoint(x, y)):
                return fig
        return None

    def addLabels2Fig(self):
        for label in self.labels:
            figure = self.insideFigure(label.x(), label.y())
            if figure:
                figure.labels.append(label)
                if not figure in self.figures:
                    self.figures.append(figure)

    def removeBoundingFig(self):
        # select the biggest Polygon => the bounding one
        # it finds the biggest rectangle for now
        max_area = 0
        max_fig = None
        pen_blue = QPen(Qt.blue)
        pen_blue.setWidth(50)

        for fig in self.figures_all:
            if len(fig.polygon()) == 5 and fig.polygon().isClosed():
                x0, y0 = fig.polygon().at(0).x(), fig.polygon().at(0).y()
                x1, y1 = fig.polygon().at(1).x(), fig.polygon().at(1).y()
                x2, y2 = fig.polygon().at(2).x(), fig.polygon().at(2).y()
                if ((x1-x0)**2 + (y1-y0)**2) * ((x2-x1)**2 + (y2 - y1)**2) > max_area:
                    max_area = ((x1-x0)**2 + (y1-y0)**2) * ((x2-x1)**2 + (y2 - y1)**2)
                    max_fig = fig

        # self.figures.remove(max_fig)
        self.figures_all.remove(max_fig)
        max_fig.setPen(pen_blue)

    def colorFigure(self, index):
        pen_red = QPen(Qt.red)
        pen_red.setWidth(50)
        self.figures[index].setPen(pen_red)
        s = ''
        for lb in self.figures[index].labels:
            s += " " + lb.toPlainText()
        print(s)

    def mousePressEvent(self, event):
        # print("Click: ",event.scenePos().x(), event.scenePos().y())
        fig = self.insideFigure(event.scenePos().x(), event.scenePos().y())

        #if not inside a figure
        if not fig:
            return

        pen_green = QPen(Qt.green)
        pen_green.setWidth(50)
        brush_green = QBrush()
        brush_green.setColor(Qt.green)
        brush_green.setStyle(Qt.SolidPattern)

        s = ''
        for lb in fig.labels:
            s += " " + lb.toPlainText()
        fig.setBrush(brush_green)
        print(s)
        self.export(s)

    def export(self, s):
        f_in = open("template.txt", "r")
        f_out = open("temp_1.txt", "w")
        for l in f_in:
            new_l = re.sub('<label>', s, l)
            f_out.write(new_l)
            
        f_out.close()
        f_in.close()
        subprocess.call('pr.bat')
